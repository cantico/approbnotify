<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2011 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__).'/functions.php';

bab_functionality::includeOriginal('PortletBackend');

class Func_PortletBackend_ApprobNotify extends Func_PortletBackend 
{
	public function getDescription()
	{
		return approbnotify_translate("Notify approbators");
	}
	
	
	public function select($category = null)
	{

		if (isset($category))
		{
			return array();
		}

		return array(
			'ApprobNotify_0' => $this->Portlet_ApprobNotify()
		);
	}
	
	
	
	/**
	 * Get portlet definition instance
	 * @param	string	$portletId			portlet definition ID
	 *
	 * @return portlet_PortletDefinitionInterface
	 */
	public function getPortletDefinition($portletId)
	{
		return $this->Portlet_ApprobNotify();
	}
	
	
	public function Portlet_ApprobNotify()
	{
		return new PortletDefinition_ApprobNotify();
	}
}









class PortletDefinition_ApprobNotify implements portlet_PortletDefinitionInterface
{

	private $addon;

	public function __construct()
	{
		$this->addon = bab_getAddonInfosInstance('approbnotify');
	}

	public function getId()
	{
		return 'ApprobNotify_0';
	}


	public function getName()
	{
		return approbnotify_translate("Notify approbators");
	}


	public function getDescription()
	{
		return approbnotify_translate("This portlet display a link to the approbations page if the user need to approve something");
	}


	public function getPortlet()
	{
		return new Portlet_ApprobNotify();
	}

	/**
	 * Returns the widget rich icon URL.
	 * 128x128 ?
	 *
	 * @return string
	 */
	public function getRichIcon()
	{
		return '';
	}


	/**
	 * Returns the widget icon URL.
	 * 16x16 ?
	 *
	 * @return string
	 */
	public function getIcon()
	{
		return '';
	}

	/**
	 * Get thumbnail URL
	 * max 120x60
	 */
	public function getThumbnail()
	{
		return '';
	}

	public function getConfigurationActions()
	{
		return array();
	}

	public function getPreferenceFields()
	{
		return array();
	}
}







class Portlet_ApprobNotify extends Widget_Item implements portlet_PortletInterface
{
	private $options = array();

	
	/**
	* @param Widget_Canvas	$canvas
	* @return string
	*/
	public function display(Widget_Canvas $canvas)
	{
		$n = $this->waitingItemCount();
		
		if ($n === 0)
		{
			return '';
		}
		
		$W = bab_Widgets();
		
		$title = $W->Label(approbnotify_translate('Approbations'))->addClass('title')->colon();
		$num = $W->Link($n, '?tg=approb')->addClass('number');
		
		$addon = bab_getAddonInfosInstance('approbnotify');
		
		$layout = $W->FlowItems($title, $num)->addClass('approbnotify-portlet');
		
		return $this->getStyles().
			$layout->display($canvas).
			$canvas->loadScript($layout->getId(), $addon->getTemplatePath().'script.jquery.js');
	}
	
	/**
	 * @return int
	 */
	private function waitingItemCount()
	{
		require_once $GLOBALS['babInstallPath'].'utilit/eventwaitingitems.php';
		$event = new bab_eventWaitingItemsCount;
		
		bab_fireEvent($event);
		
		return $event->getTotalCount();
	}
	
	
	
	private function getStyles()
	{
		return '
		
		<style type="text/css" scoped>
		
		
		.approbnotify-portlet {
			
			margin-top:-100px; 
			padding:5px 0;
		}
		
		
		.approbnotify-portlet .title {
			opacity:.5;
			font-weight:bold;
			vertical-align:middle;
			display:inline-block;
		}
		
		.approbnotify-portlet .number {
			border: 1px solid #9E6014;
			padding:2px 4px;
			margin:0 5px;
			background:#FEEFB3;
			display:inline-block;
			vertical-align:middle;
			color:#000;
			
			-webkit-transition: all .3s ease-out;
			-moz-transition: all .3s ease-out;
			-o-transition: all .3s ease-out;
			transition: all .3s ease-out;
		}
		
		.approbnotify-portlet .number:hover {
			background:#000;
			color:#fff;
			border: 1px solid #ccc;
		}
		
		</style>
		
		';
	}
	
	
	public function getPortletDefinition()
	{
		return new PortletDefinition_ApprobNotify();
	}
	
	/**
	* receive current user configuration from portlet API
	*/
	public function setPreferences(array $configuration)
	{
		$this->options = $configuration;
	}
	
	public function setPreference($name, $value)
	{
		$this->options[$name] = $value;
	}
	
	public function setPortletId($id)
	{
	
	}
}
