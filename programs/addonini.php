;<?php /*

[general]
name							="approbnotify"
version							="0.4.1"
addon_type						="EXTENSION"
encoding						="UTF-8"
mysql_character_set_database	="latin1"
description						="Notify approbators in a portlet"
description.fr					="Module fournissant un portlet de notification des approbations en attente"
long_description.fr             ="README.md"
delete							=1
ov_version						="8.0.99"
php_version						="5.1.0"
addon_access_control			="0"
author							="Cantico"
mysql_character_set_database	="latin1,utf8"
icon							="bell.png"
tags							="extension,default,portlet"

[addons]

widgets						=">=1.0.31"
LibTranslate				=">=1.12.0rc3.01"

;*/