## Notifications pour les approbateurs ##

[![SensioLabsInsight](https://insight.sensiolabs.com/projects/44427320-2ac6-4466-8f9e-39c006a0574c/mini.png)](https://insight.sensiolabs.com/projects/44427320-2ac6-4466-8f9e-39c006a0574c)

Propose un portlet indiquant le nombre d'approbations en attente par l'utilisateur connecté.