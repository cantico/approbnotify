jQuery('.approbnotify-portlet').ready(function() {
	setTimeout(function() {
		jQuery('.approbnotify-portlet').css('margin-top', 0);
		jQuery('.approbnotify-portlet').effect('slide', { direction:'up'}, 300, function() {
			jQuery('.approbnotify-portlet .number').effect("bounce", { distance:30, times:2 }, 300);
		});
	}, 1000);
});