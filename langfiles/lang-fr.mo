��          <      \       p      q      ~   Z   �   F  �      3     @  f   Z                   Approbations Notify approbators This portlet display a link to the approbations page if the user need to approve something Project-Id-Version: absences
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-10-17 17:54+0100
PO-Revision-Date: 2013-10-17 17:55+0100
Last-Translator: de Rosanbo Paul <paul.derosanbo@cantico.fr>
Language-Team: Cantico <paul.derosanbo@cantico.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: iso-8859-1
X-Poedit-Basepath: ../
X-Poedit-KeywordsList: approbnotify_translate;approbnotify_translate:1,2
X-Generator: Poedit 1.5.4
X-Poedit-SearchPath-0: programs
 Approbations Notifier les approbateurs Ce portlet affiche un lien vers la page des approbations si l'utilisateur doit approuver quelque chose 